import React from 'react';
import './App.css';
import axios from "axios";
import type { AxiosPromise } from 'axios';

type Works = {
  total_hours: number;
  used_hours: number;
  projects: ProjectInfo[];
};

type ProjectInfo = {
  used_hours: number;
  total_hours: number;
  project: Project;
  invoices_sum: number;
};

type Project = {
  id: number;
  name: string;
  identifier: string;
  parent_id: number;
  status_id: number;
  updated_on: string;
  created_on: string;
  hour_price: number;
};

const API_URL = 'http://192.168.100.87/api';
const BASE_URL_TEAM_INFO = '/marketing-time-stats/by-parent?projectId=';

const apiInstance = axios.create({
  baseURL: API_URL,
});


function App() {
  const [projects, setProjects] = React.useState<Works>();

  const getTeamInfoByMonth = (projectId: number, month: number, year: number): AxiosPromise<Works> => {
    return apiInstance.get(
      `${BASE_URL_TEAM_INFO + projectId}&month=${month}&year=${year}`,
    );
  };

  React.useEffect(() => {    
    getTeamInfoByMonth(3369,1,2023).then(({ data }) => {
      setProjects(data);
    });
  }, []);

  if (!projects) {
    return <>Загрузка...</>;
  }
  return (
    <div className="App">
      <header className="App-header">
        Всего часов: {projects.total_hours}
        
      </header>
    </div>
  );
}

export default App;
